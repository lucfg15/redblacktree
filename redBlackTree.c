//LUCAS FRISON GONCALVES

#include <stdio.h>
#include <stdlib.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
  struct arvoreRB *pai;
} ArvoreRB;

ArvoreRB *raiz = NULL;

int eh_no_vermelho(ArvoreRB * no){
  if(!no) return BLACK;
  return(no->cor == RED);
}

int buscar (ArvoreRB *a, int v) {
  if (a == NULL) { return 0; } /*Nao achou*/
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } /*Achou*/
}

void in_order(ArvoreRB *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

void print(ArvoreRB * a,int spaces){
  int i;
  for(i=0;i<spaces;i++) printf(" ");
  if(!a){
    printf("//\n");
    return;
  }

  printf("%d\n", a->info);
  print(a->esq,spaces+2);
  print(a->dir,spaces+2);
}

ArvoreRB *inserir(ArvoreRB *arv, ArvoreRB *no) {
    if (!arv) {
        raiz = no;
        return arv;
    }    
    if (arv->dir->cor == RED && arv->esq->cor == BLACK)
        rotacaoEsq(arv);
    else if (arv->esq->cor == RED && arv->esq->esq->cor == RED)
        rotacaoDir(arv);
    else {
        int corTempDir = arv->dir->cor;
        arv->dir->cor = arv->esq->cor;
        arv->esq->cor = corTempDir;
    }  
    if (inserir(arv->esq, no) || inserir(arv->dir, no))
        return arv;
}

void remover(ArvoreRB *arv, ArvoreRB *no) {
    ArvoreRB *noTemp = no;
    ArvoreRB *x;
    int corNo = noTemp->cor;
    if(no->esq == arv == NULL) {
    x = no->dir;
    }
    else if(no->dir == arv == NULL) {
    x = no->esq;
    }
    else {
    corNo = noTemp->cor;
    x = noTemp->dir;
    if(noTemp->pai == no) {
        x->pai = no;
    }
    else {
        noTemp->dir = no->dir;
        noTemp->dir->pai = noTemp;
    }
    noTemp->esq = no->esq;
    noTemp->esq->pai = noTemp;
    noTemp->cor = no->cor;
    }
}

void rotacaoDir(ArvoreRB *arv) {
    ArvoreRB *esquerda = arv->esq;
    arv->esq = esquerda->dir;
    if (arv->esq)
        esquerda->esq->pai = arv;
    esquerda->pai = arv->pai;
    if (!arv->pai)
        raiz = esquerda;
    else if (arv == esquerda->pai->esq)
        arv->pai->esq = esquerda;
    else
        arv->pai->dir = esquerda;
    esquerda->dir = arv;
    esquerda->pai = esquerda;
}

void rotacaoEsq(ArvoreRB *arv) {
    ArvoreRB *direita = arv->dir;
    arv->dir = direita->esq;
    if (arv->dir)
        arv->dir->pai = arv;
    direita->pai = arv->pai;
    if (!arv->pai)
        raiz = direita;
    else if (arv == arv->pai->esq)
        arv->pai->esq = direita;
    else
        arv->pai->dir = direita;
    direita->esq = arv;
    arv->pai = direita;
}

int main(){
    ArvoreRB * a;
}

